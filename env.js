const ENV = {
  ipushpull: {
    api_url: "https://test.ipushpull.com/api",
    web_url: "https://test.ipushpull.com",
    signup_url: 'https://test.ipushpull.com/pricing',
    docs_url: "https://test.ipushpull.com/docs",
    billing_url: "https://test.ipushpull.com/billing",
    help_url: "https://support.ipushpull.com",
    auth_url: "https://test-latest.ipushpull.com/pages",
    app_url: "https://test-latest.ipushpull.com/pages",
    embed_url: "https://test.ipushpull.com/embed",
    chat_url:
      "https://test.ipushpull.com/embedded/integrations/symphony/chat.html",
    api_key: "LC8aOmGqqydL3n8HyKaz8ue34M13WPux2zyuBAl1",
    api_secret:
      "kXXRR7jsoEm6JgjvBDYGca1YlGIDynuxpM9kiu9GTlqzLtmCvNWLm3pmzWUc6Uptp4DwvS1kLlC6mh5PHGGnKRNh0eBPG1IIUB5NtsEY1oSgqLcteNCdGBLnIwnOO7SY",
    hsts: false,
    storage_prefix: "ipp_test",
    transport: "",
    client_version: "test-1.0.0",
  },
  clients: {
    ipushpull: {
      app: {
        auth: {
          ipushpull: {
            client_version: "auth-1.0.0",
          },
          images: [
            "/auth/backgrounds/01.jpg",
            "/auth/backgrounds/02.jpg",
            "/auth/backgrounds/03.jpg",
          ],
        },
        embed: {
          ipushpull: {
            client_version: "embed-test",
          },
          plugins: true,
        },
        mobile: {
          ipushpull: {
            storage_prefix: "ipp_test_mobile",
            client_version: "mobile-test",
            api_key: "pFQESFlOWUaWknNTgmHfJ7sn2XtptJJhoOmbwlYR",
            api_secret:
              "BGm9d2dFx01dyMUa9LHyNRAomvlcR3rVHOYsvL1WeC9MXQFocEzGLgohPP3O1s7Bd4dLUJRiz8T2oD2dSfvpaQQhAWc15xpURkX3IFakJYS9QhexJjKODVyG9PzXMBja",
          },
          client_application: "mobile-test",
        },
        client: {
          demo: {
            text: "<h3>Welcome to ipushpull</h3><p>A no-code data-driven workflow platform, delivering your data wherever and whenever it's needed.</p>",
            demo_url: "https://ipushpull.com/123494576",
            demo_btn_text: "Test drive our interactive demo",
            video_url: "https://player.vimeo.com/video/475862606?autoplay=1",
            video_btn_text: "Watch our platform overview video" 
          },
          ipushpull: {
            _api_url: "https://test.ipushpull.com/api",
            client_version: "client-test",
          },
          stripe: {
            key: "pk_test_fK2oqzZxVdpohta5kABg4j9P",
          },
          states_menu: true,
          pinned_folders: true,
          formula_bar: true,
          plugins: true,
          page_schema_builder: false,
          page_query: true,
          notifications: true,
          lsd: true,
          dod: true,
          caw: true,
          row_selection: true,
          release_notes: "https://support.ipushpull.com",
          caw_popup_url: "https://test.ipushpull.com/ipp-user/clientapp-service-custom-applications",
          experimental_features: true,      
          theme: {
            upgrade: {
              version: "2021.3.3",
              type: "major",
              release: {
                label: "Find out more",
                help_doc: "https://support.ipushpull.com"
              },           
              message: "<p>There's a new version of the ipushpull app available, which includes:</p><ul><li>Streamlined onboarding for new and invited users</li><li>Microsoft Teams and Slack integrations coming soon - get in touch for a preview</li></ul>",
              logo: {
                light: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-new.svg',
                dark: 'https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-white.svg'
              },
            },
            ui: {
              nav_home_startup: true,
              workspaces_show_views: true,
              workspaces_page_update_format: "HH:mm:ss",
              workspaces_tab_linking: true,
              admin_user_startup: "client|mobile|symphony|teams",
              workspaces_disable_iframes: false,
              workspaces_tab_linking: true,
              admin_sub_organizations: true,
              admin_tags: true,
              admin_routing_rules: true
            },      
            mobile_ui: {
              nav_home_startup: true,
              workspaces_show_views: true,
              workspaces_page_update_format: "HH:mm:ss",
            },
            custom: true,
          },
          platform_explanation: [
            {
              title: 'Right Data',
              description: {
                '1': 'Organize your data in folders and pages',
                '2': 'Update your pages from Excel, your database or via our API',
                '3': 'Create custom views on your data'
              },
              icon: 'table-large',
              image_light_mode: 'img/Right-Data-dark.png',
              image_dark_mode: 'img/Right-Data-light.png',
              to: 'https://ipushpull.atlassian.net/l/c/FfB21PyN',
            },
            {
              title: 'Right Time',
              description: {
                '1': 'View live and streaming data',
                '2': 'Pull data on demand into chat',
                '3': 'Send data-driven notifications to chat, SMS and email'
              },
              icon: 'bell',
              image_light_mode: 'img/Right-Time-dark.png',
              image_dark_mode: 'img/Right-Time-light.png',
              to: 'https://ipushpull.atlassian.net/l/c/rZoig1TW',
            },
            {
              title: 'Right Place',
              description: {
                '1': 'Share your data with colleagues and clients',
                '2': 'Control their access',
                '3': 'View your data on the web, in Excel and in chat'
              },
              image_light_mode: 'img/Right-Place-dark.png',
              image_dark_mode: 'img/Right-Place-light.png',
              icon: 'web',
              to: 'https://ipushpull.atlassian.net/l/c/FiK1r5aX',
            }
          ],
          services: [
            {
              name: "Desktop",
              key: "ipp",
              serviceType: ["lsd"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
            },
            {
              name: "Mobile",
              key: "mobile",
              serviceType: ["lsd"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
            },
            {
              name: "Excel Add-in",
              key: "excel",
              serviceType: ["lsd"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
            },
            {
              name: "Microsoft Teams",
              key: "teams",
              botKey: "teamsBot",
              serviceType: ["lsd", "dod", "ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
            },
            {
              name: "Symphony",
              key: "symphony",
              botKey: "symphonyBot",
              serviceType: ["lsd", "dod", "ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
            },
            {
              name: "Slack",
              key: "slack",
              serviceType: ["dod", "ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/slack.png",
            },
            {
              name: "WhatsApp",
              key: "whatsapp",
              serviceType: ["ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/whatsapp.png",
            },
            {
              name: "SMS",
              key: "sms",
              serviceType: ["ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/sms.png",
            },
            {
              name: "Email",
              key: "email",
              serviceType: ["ddn"],
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/email.png",
            },
          ],
          data_sources: [
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
                  iframe: "https://test.ipushpull.com/help.php?id=647790593",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 13,
              name: "Database",
              key: "db",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/db.svg",
              // help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
                  iframe: "https://test.ipushpull.com/help.php?id=148865025",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: -1,
              name: "API",
              key: "api",
              logo: "",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/xsgmc85x",
                  iframe: "https://test.ipushpull.com/help.php?id=681607177",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            }
          ],
          client_apps: [
            {
              id: 2,
              name: "Desktop App",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                  iframe: "https://test.ipushpull.com/help.php?id=681705505",
                },
                dod: {
                  desk: "",
                  iframe: ""
                }
              },
              appType: ['lsd']
            },
            {
              id: 41,
              name: "Mobile App",
              key: "mobile",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/qxv9HZC1",
                  iframe: "https://test.ipushpull.com/help.php?id=650248196",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
                  iframe: "https://test.ipushpull.com/help.php?id=156074055",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            },
            {
              id: 38,
              name: "Microsoft Teams App",
              key: "teams",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
                  iframe: "https://test.ipushpull.com/help.php?id=678494209",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/aut0u3UB",
                  iframe: "https://test.ipushpull.com/help.php?id=741670913",
                }
              }
            },
            {
              id: 42,
              name: "Microsoft Teams Bot",
              key: "teamsBot",
              logo: "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.svg",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
                  iframe: "https://test.ipushpull.com/help.php?id=678494209",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/aut0u3UB",
                  iframe: "https://test.ipushpull.com/help.php?id=741670913",
                }
              }
            },
            {
              id: 7,
              name: "Slack Bot",
              key: "slack",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/slack.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/NcSUB3rn",
                  iframe: "https://test.ipushpull.com/help.php?id=1442807809",
                }
              }
            },
            {
              id: 14,
              name: "Symphony App",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
                  iframe: "https://test.ipushpull.com/help.php?id=695959587",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/DeKPs5es",
                  iframe: "https://test.ipushpull.com/help.php?id=742162433",
                }
              }
            },
            {
              id: 43,
              name: "Symphony Bot",
              key: "symphonyBot",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ['dod'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
                  iframe: "https://test.ipushpull.com/help.php?id=695959587",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/DeKPs5es",
                  iframe: "https://test.ipushpull.com/help.php?id=742162433",
                }
              }
            },
            {
              id: -1,
              name: "Client API",
              key: "api",
              logo: "",
              appType: ['lsd'],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/xsgmc85x",
                  iframe: "https://test.ipushpull.com/help.php?id=681607177",
                },
                dod: {
                  desk: "",
                  iframe: "",
                }
              }
            }
          ],          
        },
        teams: {
          ipushpull: {
            storage_prefix: "ipp_test_teams",
            client_version: "teams-test",
            help_url: "https://support.ipushpull.com/teams",
            api_key: "xIsvUNH1URwllURqhloJEh53bqbvR7JOLmqRVzAF",
            api_secret:
              "2rYpSzRIhRcGgDHuj8Jnn9rgzbOHlVqBpXVJO7KTK2QiouhpG97oISDSTc54KwZw8sRrrXCDOIx6orO0PFvNVxmngCnE4mw8R6kbkqAEr3ICqHhTgmAsnKN57CJpO613",
          },
          guides: [
            { version: '2021.3.1', name: 'finder', content: 'Click here to choose a live data page', position: 'bottom' },
            { version: '2021.3.1', name: 'toolbar', content: 'Filter and sort your data and change settings here', position: 'bottom' },
            { version: '2021.3.1', name: 'footer', content: 'For help and support choose an option', position: 'top' },
          ],        
          integration: {
            urls: {
              app: "https://test.ipushpull.com/roapp/",
              module:
                "https://test.ipushpull.com/embedded/integrations/teams/app.html",
              remove:
                "https://test.ipushpull.com/embedded/integrations/teams/tabremove.html",
              logout:
                "https://test.ipushpull.com/embedded/integrations/teams/tabpersonal.html",
            }
          },
          client_application: "ipp-teams-app",
          more_info_url: "https://support.ipushpull.com"          
        },
        symphony: {
          ipushpull: {
            storage_prefix: "ipp_test_symphony",
            client_version: "symphony-test",
            api_key: "symphony-embedded",
            api_secret:
              "Au3K9q3zJTJMPBf8NhYbjtsqJlqPTR8EoyOZ0fG8LQag0aVcOjrnjiVaqCvvVjvEyWXZ0OinkcAg0nTwjjMkwDuHD9Amg1B5jmxtEHE2HftFrT0m59LHUEMErldyYu6t",
          },
          workflow: {
            novation: true,
          },
          client_application: "symphony-embedded-app",
          pinned_folders: true,
          destinations: true,
          plugins: true,
          formula_bar: true,
          disable_email_destinations: true,
          symphony_streams: true,
          message_icon:
            "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
          share_icon:
            "https://ipushpull.s3.amazonaws.com/static/prd/ipushpull-thumb.png",            
          integration: {
            id: "nwm-test-app",
            name: "ipushpull",
            title: "ipushpull - test",
            urls: {
              app: "https://test.ipushpull.com/syapp/",
              module:
                "https://test.ipushpull.com/embedded/integrations/symphony/app.html",
              blank:
                "https://test.ipushpull.com/embedded/integrations/symphony/blank.html",
            },
            icons: {
              ui:
                "https://ipushpull.s3.amazonaws.com/static/prd/icon-16.png",
              ui_2:
                "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",                
              nav: "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
            },
            test: true,
          },
          about: {
            demo: "",
            integrations: "https://ipushpull.com/integrations",
            terms: "https://ipushpull.com/terms",
            privacy: "https://ipushpull.com/terms#privacy",
            contact: "https://ipushpull.com/contact",
            more_info:
              "https://ipushpull.com/client-symphony",
          },
          services: [
            {
              title: "Live & Streaming Data",
              description:
                "Deliver real-time data into Symphony and other desktop and mobile apps for your team or clients",
              class: "border-color-ipp-secondary",
              color: "ipp-secondary",
              icon: "table-large",
              more_info: "https://ipushpull.com/service-live-streaming",
            },
            {
              title: "Data on Demand",
              description:
                "Pull data on demand from live systems into Symphony, or send updates with ipushpull�s configurable bot service",
              class: "border-color-ipp-tertiary",
              color: "ipp-tertiary",
              icon: "robot",
              more_info: "https://ipushpull.com/service-data-on-demand",
            },
            {
              title: "Data-driven Notifications",
              description:
                "Automated, live data-driven alerts sent to your teams or clients on desktop and mobile apps",
              class: "border-color-ipp-primary",
              color: "ipp-primary",
              icon: "bell",
              more_info: "https://ipushpull.com/service-notifications",
            },
            {
              title: "Custom Applications & Workflows",
              description:
                "Customised solutions and workflows, integrated and white-labelled to order",
              class: "border-color-ipp-yellow",
              color: "ipp-yellow",
              icon: "flow-chart",
              more_info: "https://ipushpull.com/service-custom-applications",
            },
          ],          
        },
        views: {
          ipushpull: {
            client_version: "views-test",
            help_url: "https://support.ipushpull.com",
          },
          client_application: "ipp-views-app",
          more_info_url: "https://support.ipushpull.com"
        },        
      },
    },
    euromoney: {
      app: {
        symphony: {
          hide_route: true,
          __hide_page_border: true,
          plugins: true,
          client_application: "euromoney",
          ipushpull: {
            help_url: "",
            storage_prefix: "euromoney_test_symphony",
            client_version: "symphony-test",
            api_key: "Nbkt8xz0XmE9lZKAsRWlHKET5fw6RkB7roFzWlqV",
            api_secret:
              "G6KO7UkTL8UVvosGQY2GO2U6CP3okCB3cXF32MGckPTaF9BK4qiqewLTwsdC3YwWg1g3LQmuSgLAwSY2na3rpLzceRfKP1FjDiIKp3Sdac5CuclsEx3kp7pWhMe3hUzX",
          },
          destinations: true,
          disable_email_destinations: true,
          symphony_streams: true,          
          message_icon:
            "https://ipushpull.s3.amazonaws.com/static/prd/icon-32.png",
          share_icon:
            "https://ipushpull.s3.amazonaws.com/static/prd/ipushpull-thumb.png",
          integration: {
            id: "euromoney-test",
            title: "Euromoney TRADEDATA",
            name: "Euromoney TRADEDATA",
            urls: {
              app: "https://test.ipushpull.com/roapp/",
              module:
                "https://test.ipushpull.com/embedded/integrations/symphony/app.html",
              blank:
                "https://test.ipushpull.com/embedded/integrations/symphony/blank.html",
            },
            icons: {
              ui:
                "https://ipushpull.s3.amazonaws.com/static/prd/EMTD_icon_32x16.png",
              nav:
                "https://ipushpull.s3.amazonaws.com/static/prd/EMTD_icon_32x32.png",
            },
            test: true,
          },
          theme: {
            color: {
              primary: "#361744",
              secondary: "#8a70a0",
            },
            header: {
              icon: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-light.svg",
                dark:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-dark.svg",
              },
            },
            home: {
              logo: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-light.svg",
                dark:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-dark.svg",
              },
            },
          },
          tos_url: "https://www.euromoneytradedata.com/terms-and-conditions",
          privacy_url: "https://www.euromoneytradedata.com/privacy-policy",
          more_info_url: "https://www.euromoneytradedata.com/",
        },
        teams: {
          ipushpull: {
            storage_prefix: "euromoney_test_teams",
            client_version: "teams-test",
            help_url: "",
            api_key: "xIsvUNH1URwllURqhloJEh53bqbvR7JOLmqRVzAF",
            api_secret:
              "2rYpSzRIhRcGgDHuj8Jnn9rgzbOHlVqBpXVJO7KTK2QiouhpG97oISDSTc54KwZw8sRrrXCDOIx6orO0PFvNVxmngCnE4mw8R6kbkqAEr3ICqHhTgmAsnKN57CJpO613",
          },
          integration: {
            urls: {
              app: "https://test.ipushpull.com/roapp/",
              module:
                "https://test.ipushpull.com/embedded/integrations/teams/app.html?client=euromoney",
              remove:
                "https://test.ipushpull.com/embedded/integrations/teams/tabremove.html",
            },
          },
          theme: {
            title: "Euromoney TRADEDATA",
            color: {
              primary: "#361744",
              secondary: "#8a70a0",
            },
            header: {
              icon: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-light.svg",
                dark:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-dark.svg",
              },
            },
            home: {
              logo: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-light.svg",
                dark:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-dark.svg",
              },
            },
          },
          client_application: "ipp-teams-app",
          tos_url: "https://www.euromoneytradedata.com/terms-and-conditions",
          privacy_url: "https://www.euromoneytradedata.com/privacy-policy",
          more_info_url: "https://www.euromoneytradedata.com/",
        },
        openfin: {
          hide_route: true,
          ipushpull: {
            storage_prefix: "euromoney_test_openfin",
            client_version: "euromoney-openfin-test",
            help_url: "",
            api_key: "xIsvUNH1URwllURqhloJEh53bqbvR7JOLmqRVzAF",
            api_secret:
              "2rYpSzRIhRcGgDHuj8Jnn9rgzbOHlVqBpXVJO7KTK2QiouhpG97oISDSTc54KwZw8sRrrXCDOIx6orO0PFvNVxmngCnE4mw8R6kbkqAEr3ICqHhTgmAsnKN57CJpO613",
          },
          integration: {
            urls: {
              app: "https://test.ipushpull.com/roapp/",
              module:
                "https://test.ipushpull.com/embedded/integrations/teams/app.html?client=euromoney",
              remove:
                "https://test.ipushpull.com/embedded/integrations/teams/tabremove.html",
            },
          },
          theme: {
            title: "Euromoney TRADEDATA",
            color: {
              primary: "#361744",
              secondary: "#8a70a0",
            },
            header: {
              icon: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-light.svg",
                dark:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-square-dark.svg",
              },
            },
            home: {
              logo: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-light.svg",
                dark:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/euromoney/euromoney-tradedata-dark.svg",
              },
            },
          },
          client_application: "ipp-teams-app",
          tos_url: "https://www.euromoneytradedata.com/terms-and-conditions",
          privacy_url: "https://www.euromoneytradedata.com/privacy-policy",
          more_info_url: "https://www.euromoneytradedata.com/",
        },
      },
    },
  },
  sso_only: false,
  sso_url: "https://test.ipushpull.com/api/1.0/sso/TQ6M5Qe9GJXqm6coP4pBD6/",
  on_prem: false,
  billing: true,
  eu_vat_countries: [
    "AT",
    "BE",
    "BG",
    "HR",
    "CY",
    "CZ",
    "DK",
    "EE",
    "FI",
    "FR",
    "DE",
    "EL",
    "HU",
    "IE",
    "IT",
    "LV",
    "LT",
    "LU",
    "MT",
    "NL",
    "PL",
    "PT",
    "RO",
    "SK",
    "SI",
    "ES",
    "SE",
  ],
  help_docs: {
    livestreaming_data_source_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
      iframe: "https://test.ipushpull.com/help.php?id=647790593",
    },
    livestreaming_data_source_db: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
      iframe: "https://test.ipushpull.com/help.php?id=148865025",
    },
    livestreaming_data_source_api: {
      desk: "https://ipushpull.atlassian.net/wiki/x/CYCgK",
      iframe: "https://test.ipushpull.com/help.php?id=681607177",
    },
    livestreaming_client_app_ipp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
      iframe: "https://test.ipushpull.com/help.php?id=681705505",
    },
    livestreaming_client_app_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
      iframe: "https://test.ipushpull.com/help.php?id=156074055",
    },
    livestreaming_client_app_symphony: {
      desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
      iframe: "https://test.ipushpull.com/help.php?id=695959587",
    },
    livestreaming_client_app_wp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/a4CBKQ",
      iframe: "https://test.ipushpull.com/help.php?id=696352875",
    },
    livestreaming_client_app_teams: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
      iframe: "https://test.ipushpull.com/help.php?id=678494209",
    },
    notifications_troubleshooting: {
      desk: "https://ipushpull.atlassian.net/l/c/g1e9zKzm",
      iframe: "https://test.ipushpull.com/help.php?id=565608455",
    },
    notifications_builder_excel_data_source: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDmI",
      iframe: "https://test.ipushpull.com/help.php?id=551976961",
    },
    notifications_builder_msteams_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/H4BeHg",
      iframe: "https://test.ipushpull.com/help.php?id=509509663",
    },
    notifications_builder_slack_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/JgBfHg",
      iframe: "https://test.ipushpull.com/help.php?id=509542438",
    },
    notifications_builder_symphony_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/LwBfHg",
      iframe: "https://test.ipushpull.com/help.php?id=509542447",
    },
    symphony_overview: {
      desk: "https://ipushpull.atlassian.net/wiki/x/UYCHDQ",
      iframe: "https://test.ipushpull.com/help.php?id=226984017",
    },
    user_authoriziations: {
      desk: "https://ipushpull.atlassian.net/l/c/GApuWLAA",
      iframe: "https://test.ipushpull.com/help.php?id=631701505",
    },
    page_views: {
      desk: "https://ipushpull.atlassian.net/l/c/sxEHKpxC",
      iframe: "https://test.ipushpull.com/help.php?id=1431175169",
    },
    workspaces: {
      desk: "https://ipushpull.atlassian.net/l/c/3BpmrK5E",
      iframe: "https://test.ipushpull.com/help.php?id=171442177",
    },    
  },
  hubspot_key: "38de1cb8-d7cb-4e9e-96e4-0438735e3811"  
};
window.hsConversationsSettings = {
  loadImmediately: false,
};